//Java Program to Illustrate Kafka Consumer
package com.example.demo.kafka.consumer;

//Importing required classes
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.stereotype.Component;

@Component

//Class
public class KafkaConsumer {

	@KafkaListener(topics = "luatvq_test", groupId = "luatvq", containerFactory = "concurrentKafkaListenerContainerFactory")

	// Method
	public void consume(String message) {
		// Print statement
		System.out.println("message = " + message);
	}
}
