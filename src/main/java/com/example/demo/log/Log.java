package com.example.demo.log;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "LOGS")
public class Log {

    private long id;
    private String app;
    private String lv;
    private String message;
    private String details;
    private Date created;
 
    public Log() {
  
    }
 
    public Log(String app, String level, String message, String details, Date created) {
        this.app = app;
        this.lv = level;
        this.message = message;
        this.details = details;
        this.created = created;
    }
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
 
    @Column(name = "APP", nullable = false)
    public String getApp() {
        return app;
    }
    public void setApp(String firstName) {
        this.app = firstName;
    }
 
    @Column(name = "LV", nullable = false)
    public String getLevel() {
        return lv;
    }
    public void setLevel(String level) {
        this.lv = level;
    }
 
    @Column(name = "MESSAGE", nullable = false)
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    
    @Column(name = "DETAILS", nullable = false)
    public String getDetails() {
        return details;
    }
    public void setDetails(String details) {
        this.details = details;
    }
    
    @Column(name = "CREATED", nullable = false)
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + app + ", lastName=" + lv + ", emailId=" + message + ", details=" + details
    + "]";
    }
 
}