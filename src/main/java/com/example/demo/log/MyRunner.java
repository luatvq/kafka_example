package com.example.demo.log;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);

	@Autowired
	private LogRepository logRepository;

	@Override
	public void run(String... args) throws Exception {

		logRepository.save(new Log("app", "level", "ramesh@gmail.com", "details", new Date()));
		logRepository.save(new Log("app", "level", "tom@gmail.com", "details", new Date()));
		logRepository.save(new Log("app", "level", "john@gmail.com", "details", new Date()));
		logRepository.save(new Log("app", "level", "stark@gmail.com", "details", new Date()));

		logger.info("# of employees: {}", logRepository.count());

		logger.info("All employees unsorted:");
		List<Log> employees = logRepository.findAll();
		logger.info("{}", employees);

		logger.info("------------------------");

//        logger.info("All employees sorted by name in descending order");
//        List<Log> sortedEmployees = logRepository.findAll(new Sort(Sort.Direction.DESC, "firstName"));
//        logger.info("{}", sortedEmployees);

//        logger.info("------------------------");

//        logger.info("Deleting all employees");
//        logRepository.deleteAllInBatch();

		logger.info("# of employees: {}", logRepository.count());
	}
}